// ==UserScript==
// @name         blur filter
// @namespace    tbui@lyonscg
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https*://www.reddit.com/*
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// ==/UserScript==
(function () {
    'use strict';
     //the rest of the function
  }());
  
  $(document).ready(function(){
      $(".thing").addClass('blur');
      $(".blur").css({
          "-webkit-filter": "blur(5px)", /* Chrome, Safari, Opera */
          //"filter": "blur(5px)"
      });
      $("a.title").on({
          mouseenter: function() {
              $(this).parents(".thing").toggleClass("active");
              if ($(this).parents(".thing.active").hasClass("active")) {
                  $(this).parents(".thing.active").css({
                      "-webkit-filter": "blur(0px)",
                      "filter": "blur(0px)"
                  });
              }
          },
          mouseleave: function() {
              $(this).parents(".thing").toggleClass("active");
  
              if ($(this).parents(".thing").not(".thing.active")) {
                  $(this).parents(".thing").addClass('blur');
              }
              $(".blur").css({
                  "-webkit-filter": "blur(5px)",
                  //"filter": "blur(5px)"
              });
          }
      });
  });
  