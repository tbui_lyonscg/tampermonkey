// ==UserScript==
// @name         map
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.google.com
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// ==/UserScript==
(function () {
    'use strict';
     //the rest of the function
  }());
  
  $(document).ready(function(){
      $('<div id="googleMap"><a id="imageLink" href="https://www.google.com/maps"></a></div>').appendTo(".content");
      $("#googleMap").css({
          'top':'50%',
          'left':'50%',
          'display':'block',
          'padding-bottom':'50px'
      });
  
      var mapImg = $('<img />', {
          id: 'google_map',
          href: 'https://www.google.com/maps',
          src:'https://maps.googleapis.com/maps/api/staticmap?center=Dallas,TX&zoom=13&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284&key=AIzaSyCGkpXHT_JmSIVaW-dO8gsPf8V-pZDP3mg',
          alt: 'New York Position'
      });
  
      mapImg.appendTo("#imageLink");
      mapImg.css({
          'margin-left':'30%'
      });
  
  });